# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Martin Schlander <mschlander@opensuse.org>, 2015, 2016, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-17 01:38+0000\n"
"PO-Revision-Date: 2019-05-30 17:17+0100\n"
"Last-Translator: Martin Schlander <mschlander@opensuse.org>\n"
"Language-Team: Danish <kde-i18n-doc@kde.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: package/contents/config/config.qml:12
#, kde-format
msgctxt "@title"
msgid "General"
msgstr "Generelt"

#: package/contents/ui/ConfigGeneral.qml:27
#, kde-format
msgctxt "@label:spinbox"
msgid "Maximum columns:"
msgstr "Maks. antal kolonner:"

#: package/contents/ui/ConfigGeneral.qml:27
#, kde-format
msgctxt "@label:spinbox"
msgid "Maximum rows:"
msgstr "Maks. antal rækker:"

#: package/contents/ui/ConfigGeneral.qml:39
#, kde-format
msgctxt "@title:group"
msgid "Appearance:"
msgstr "Udseende:"

#: package/contents/ui/ConfigGeneral.qml:41
#, kde-format
msgctxt "@option:check"
msgid "Show launcher names"
msgstr "Vis navne på programstartere"

#: package/contents/ui/ConfigGeneral.qml:46
#, kde-format
msgctxt "@option:check"
msgid "Enable popup"
msgstr "Aktivér pop-op"

#: package/contents/ui/ConfigGeneral.qml:56
#, kde-format
msgctxt "@title:group"
msgid "Title:"
msgstr "Titel:"

#: package/contents/ui/ConfigGeneral.qml:64
#, kde-format
msgctxt "@option:check"
msgid "Show:"
msgstr "Vis:"

#: package/contents/ui/ConfigGeneral.qml:80
#, kde-format
msgctxt "@info:placeholder"
msgid "Custom title"
msgstr "Brugervalgt titel"

#: package/contents/ui/IconItem.qml:177
#, kde-format
msgid "Launch %1"
msgstr ""

#: package/contents/ui/IconItem.qml:260
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Add Launcher..."
msgctxt "@action:inmenu"
msgid "Add Launcher…"
msgstr "Tilføj programstarter..."

#: package/contents/ui/IconItem.qml:266
#, fuzzy, kde-format
#| msgctxt "@action:inmenu"
#| msgid "Edit Launcher..."
msgctxt "@action:inmenu"
msgid "Edit Launcher…"
msgstr "Redigér programstarter..."

#: package/contents/ui/IconItem.qml:272
#, kde-format
msgctxt "@action:inmenu"
msgid "Remove Launcher"
msgstr "Fjern programstarter"

#: package/contents/ui/main.qml:146
#, kde-format
msgid "Quicklaunch"
msgstr "Kvikstart"

#: package/contents/ui/main.qml:147
#, kde-format
msgctxt "@info"
msgid "Add launchers by Drag and Drop or by using the context menu."
msgstr "Tilføj programstartere ved hjælp af træk og slip eller højreklik."

#: package/contents/ui/main.qml:177
#, kde-format
msgid "Hide icons"
msgstr "Skjul ikoner"

#: package/contents/ui/main.qml:177
#, kde-format
msgid "Show hidden icons"
msgstr "Vis skjulte ikoner"

#: package/contents/ui/main.qml:280
#, fuzzy, kde-format
#| msgctxt "@action"
#| msgid "Add Launcher..."
msgctxt "@action"
msgid "Add Launcher…"
msgstr "Tilføj programstarter..."

#~ msgctxt "@title:group"
#~ msgid "Arrangement"
#~ msgstr "Opstilling"

#~ msgctxt "@info:placeholder"
#~ msgid "Enter title"
#~ msgstr "Angiv titel"
