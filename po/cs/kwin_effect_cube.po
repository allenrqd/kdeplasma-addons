# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
# SPDX-FileCopyrightText: 2023 Vit Pelcak <vit@pelcak.org>
#
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-24 01:42+0000\n"
"PO-Revision-Date: 2023-11-06 10:04+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.2\n"

#: kcm/cubeeffectkcm.cpp:35
#, kde-format
msgid "KWin"
msgstr "KWin"

#: kcm/cubeeffectkcm.cpp:41 package/contents/ui/main.qml:39
#, kde-format
msgid "Toggle Cube"
msgstr "Zapnout kostku"
