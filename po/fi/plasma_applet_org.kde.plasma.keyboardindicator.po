# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Tommi Nieminen <translator@legisign.org>, 2018, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-22 00:38+0000\n"
"PO-Revision-Date: 2023-07-27 11:32+0300\n"
"Last-Translator: Tommi Nieminen <translator@legisign.org>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Keys"
msgstr "Näppäimet"

#: contents/ui/configAppearance.qml:37
#, kde-format
msgctxt ""
"@label show keyboard indicator when Caps Lock or Num Lock is activated"
msgid "Show when activated:"
msgstr "Aktivoituna näytä:"

#: contents/ui/configAppearance.qml:40
#, kde-format
msgctxt "@option:check"
msgid "Caps Lock"
msgstr "Vaihtolukko"

#: contents/ui/configAppearance.qml:47
#, kde-format
msgctxt "@option:check"
msgid "Num Lock"
msgstr "Numerolukko"

#: contents/ui/main.qml:87
#, kde-format
msgid "Caps Lock activated"
msgstr "Vaihtolukko käytössä"

#: contents/ui/main.qml:90
#, kde-format
msgid "Num Lock activated"
msgstr "Numerolukko käytössä"

#: contents/ui/main.qml:98
#, kde-format
msgid "No lock keys activated"
msgstr "Ei lukitusnäppäimiä käytössä"

#~ msgid "Num Lock"
#~ msgstr "Numerolukko"

#~ msgid "%1: Locked\n"
#~ msgstr "%1: Lukittu\n"

#~ msgid "Unlocked"
#~ msgstr "Ei lukittu"

#~ msgid "Appearance"
#~ msgstr "Ulkoasu"
