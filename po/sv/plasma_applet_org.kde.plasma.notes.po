# translation of plasma_applet_notes.po to Swedish
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2007, 2008, 2009, 2015, 2018, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_notes\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-26 01:34+0000\n"
"PO-Revision-Date: 2021-04-10 07:57+0200\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.08.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Utseende"

#: package/contents/ui/configAppearance.qml:31
#, kde-format
msgid "%1pt"
msgstr "%1 punkter"

#: package/contents/ui/configAppearance.qml:37
#, kde-format
msgid "Text font size:"
msgstr "Teckenstorlek för text:"

#: package/contents/ui/configAppearance.qml:62
#, kde-format
msgid "A white sticky note"
msgstr "En vit anteckningslapp"

#: package/contents/ui/configAppearance.qml:63
#, kde-format
msgid "A black sticky note"
msgstr "En svart anteckningslapp"

#: package/contents/ui/configAppearance.qml:64
#, kde-format
msgid "A red sticky note"
msgstr "En röd anteckningslapp"

#: package/contents/ui/configAppearance.qml:65
#, kde-format
msgid "An orange sticky note"
msgstr "En orange anteckningslapp"

#: package/contents/ui/configAppearance.qml:66
#, kde-format
msgid "A yellow sticky note"
msgstr "En gul anteckningslapp"

#: package/contents/ui/configAppearance.qml:67
#, kde-format
msgid "A green sticky note"
msgstr "En grön anteckningslapp"

#: package/contents/ui/configAppearance.qml:68
#, kde-format
msgid "A blue sticky note"
msgstr "En blå anteckningslapp"

#: package/contents/ui/configAppearance.qml:69
#, kde-format
msgid "A pink sticky note"
msgstr "En rosa anteckningslapp"

#: package/contents/ui/configAppearance.qml:70
#, fuzzy, kde-format
#| msgid "A translucent sticky note"
msgid "A transparent sticky note"
msgstr "En genomskinlig anteckningslapp"

#: package/contents/ui/configAppearance.qml:71
#, fuzzy, kde-format
#| msgid "A translucent sticky note with light text"
msgid "A transparent sticky note with light text"
msgstr "En genomskinlig anteckningslapp med ljus text"

#: package/contents/ui/main.qml:267
#, kde-format
msgid "Undo"
msgstr "Ångra"

#: package/contents/ui/main.qml:275
#, kde-format
msgid "Redo"
msgstr "Gör om"

#: package/contents/ui/main.qml:285
#, kde-format
msgid "Cut"
msgstr "Klipp ut"

#: package/contents/ui/main.qml:293
#, kde-format
msgid "Copy"
msgstr "Kopiera"

#: package/contents/ui/main.qml:301
#, kde-format
msgid "Paste"
msgstr "Klistra in"

#: package/contents/ui/main.qml:307
#, fuzzy, kde-format
#| msgid "Paste Without Formatting"
msgid "Paste with Full Formatting"
msgstr "Klistra in utan formatering"

#: package/contents/ui/main.qml:314
#, fuzzy, kde-format
#| msgid "Formatting"
msgctxt "@action:inmenu"
msgid "Remove Formatting"
msgstr "Formatering"

#: package/contents/ui/main.qml:329
#, kde-format
msgid "Delete"
msgstr "Ta bort"

#: package/contents/ui/main.qml:336
#, kde-format
msgid "Clear"
msgstr "Rensa"

#: package/contents/ui/main.qml:346
#, kde-format
msgid "Select All"
msgstr "Markera alla"

#: package/contents/ui/main.qml:474
#, kde-format
msgctxt "@info:tooltip"
msgid "Bold"
msgstr "Fetstil"

#: package/contents/ui/main.qml:486
#, kde-format
msgctxt "@info:tooltip"
msgid "Italic"
msgstr "Kursiv"

#: package/contents/ui/main.qml:498
#, kde-format
msgctxt "@info:tooltip"
msgid "Underline"
msgstr "Understruken"

#: package/contents/ui/main.qml:510
#, kde-format
msgctxt "@info:tooltip"
msgid "Strikethrough"
msgstr "Överstruken"

#: package/contents/ui/main.qml:584
#, kde-format
msgid "Discard this note?"
msgstr "Släng anteckningen?"

#: package/contents/ui/main.qml:585
#, kde-format
msgid "Are you sure you want to discard this note?"
msgstr "Är du säker på att du vill slänga anteckningen?"

#: package/contents/ui/main.qml:605
#, kde-format
msgctxt "@item:inmenu"
msgid "White"
msgstr "Vit"

#: package/contents/ui/main.qml:609
#, kde-format
msgctxt "@item:inmenu"
msgid "Black"
msgstr "Svart"

#: package/contents/ui/main.qml:613
#, kde-format
msgctxt "@item:inmenu"
msgid "Red"
msgstr "Röd"

#: package/contents/ui/main.qml:617
#, kde-format
msgctxt "@item:inmenu"
msgid "Orange"
msgstr "Orange"

#: package/contents/ui/main.qml:621
#, kde-format
msgctxt "@item:inmenu"
msgid "Yellow"
msgstr "Gul"

#: package/contents/ui/main.qml:625
#, kde-format
msgctxt "@item:inmenu"
msgid "Green"
msgstr "Grön"

#: package/contents/ui/main.qml:629
#, kde-format
msgctxt "@item:inmenu"
msgid "Blue"
msgstr "Blå"

#: package/contents/ui/main.qml:633
#, kde-format
msgctxt "@item:inmenu"
msgid "Pink"
msgstr "Rosa"

#: package/contents/ui/main.qml:637
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "Translucent"
msgctxt "@item:inmenu"
msgid "Transparent"
msgstr "Halvgenomskinlig"

#: package/contents/ui/main.qml:641
#, fuzzy, kde-format
#| msgctxt "@item:inmenu"
#| msgid "Translucent Light"
msgctxt "@item:inmenu"
msgid "Transparent Light"
msgstr "Ljus halvgenomskinlig"

#~ msgid "Background color"
#~ msgstr "Bakgrundsfärg"

#~ msgid "Toggle text format options"
#~ msgstr "Ändra alternativ för textformat"

#~ msgid "Notes Settings..."
#~ msgstr "Anteckningsinställningar..."

#~ msgid "Align left"
#~ msgstr "Vänsterjustera"

#~ msgid "Align center"
#~ msgstr "Centrera"

#~ msgid "Align right"
#~ msgstr "Högerjustera"

#~ msgid "Justified"
#~ msgstr "Anpassat"

#~ msgid "Font"
#~ msgstr "Teckensnitt"

#~ msgid "Style:"
#~ msgstr "Stil:"

#~ msgid "&Bold"
#~ msgstr "&Fetstil"

#~ msgid "&Italic"
#~ msgstr "&Kursiv"

#~ msgid "Size:"
#~ msgstr "Storlek:"

#~ msgid "Scale font size by:"
#~ msgstr "Skala teckenstorlek med:"

#~ msgid "%"
#~ msgstr "%"

#~ msgid "Color:"
#~ msgstr "Färg:"

#~ msgid "Use theme color"
#~ msgstr "Använd temats färg"

#~ msgid "Use custom color:"
#~ msgstr "Använd egen färg:"

#~ msgid "Active line highlight color:"
#~ msgstr "Markeringsfärg för aktiv rad:"

#~ msgid "Use no color"
#~ msgstr "Använd ingen färg"

#~ msgid "Theme"
#~ msgstr "Tema"

#~ msgid "Notes color:"
#~ msgstr "Anteckningarnas färg:"

#~ msgid "Spell Check"
#~ msgstr "Stavningskontroll"

#~ msgid "Enable spell check:"
#~ msgstr "Aktivera stavningskontroll:"

#~ msgid "Notes Color"
#~ msgstr "Anteckningarnas färg"

#~ msgid "General"
#~ msgstr "Allmänt"

#~ msgid "Unable to open file"
#~ msgstr "Kunde inte öppna fil"

#~ msgid "Welcome to the Notes Plasmoid! Type your notes here..."
#~ msgstr ""
#~ "Välkommen till plasmoiden Anteckningar. Skriv in dina anteckningar här..."
