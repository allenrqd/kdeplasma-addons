# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kdeplasma-addons package.
#
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-01-20 00:38+0000\n"
"PO-Revision-Date: 2019-09-29 19:46+0500\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: English <kde-i18n-doc@kde.org>\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: i18n.dat:1
#, kde-format
msgctxt "wind direction"
msgid "N"
msgstr "Ш"

#: i18n.dat:2
#, kde-format
msgctxt "wind direction"
msgid "NNE"
msgstr "ШШШ"

#: i18n.dat:3
#, kde-format
msgctxt "wind direction"
msgid "NE"
msgstr "ШШ"

#: i18n.dat:4
#, kde-format
msgctxt "wind direction"
msgid "ENE"
msgstr "ШШШ"

#: i18n.dat:5
#, kde-format
msgctxt "wind direction"
msgid "E"
msgstr "Ш"

#: i18n.dat:6
#, kde-format
msgctxt "wind direction"
msgid "SSE"
msgstr "ҶҶШ"

#: i18n.dat:7
#, kde-format
msgctxt "wind direction"
msgid "SE"
msgstr "ҶШ"

#: i18n.dat:8
#, kde-format
msgctxt "wind direction"
msgid "ESE"
msgstr "ШҶШ"

#: i18n.dat:9
#, kde-format
msgctxt "wind direction"
msgid "S"
msgstr "Ҷ"

#: i18n.dat:10
#, kde-format
msgctxt "wind direction"
msgid "NNW"
msgstr "ШШҶ"

#: i18n.dat:11
#, kde-format
msgctxt "wind direction"
msgid "NW"
msgstr "ШҒ"

#: i18n.dat:12
#, kde-format
msgctxt "wind direction"
msgid "WNW"
msgstr "ҒШҒ"

#: i18n.dat:13
#, kde-format
msgctxt "wind direction"
msgid "W"
msgstr "Ғ"

#: i18n.dat:14
#, kde-format
msgctxt "wind direction"
msgid "SSW"
msgstr "ҶҶҒ"

#: i18n.dat:15
#, kde-format
msgctxt "wind direction"
msgid "SW"
msgstr "ҶҒ"

#: i18n.dat:16
#, kde-format
msgctxt "wind direction"
msgid "WSW"
msgstr "ҒҶҒ"

#: i18n.dat:17
#, kde-format
msgctxt "wind direction"
msgid "VR"
msgstr ""

#: i18n.dat:18
#, kde-format
msgctxt "wind speed"
msgid "Calm"
msgstr "Ором"

#: package/contents/config/config.qml:13
#, kde-format
msgctxt "@title"
msgid "Weather Station"
msgstr "Дастгоҳи обу ҳаво"

#: package/contents/config/config.qml:19
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Намуди зоҳирӣ"

#: package/contents/config/config.qml:25
#, kde-format
msgctxt "@title"
msgid "Units"
msgstr "Воҳидҳо"

#: package/contents/ui/config/ConfigAppearance.qml:35
#, kde-format
msgctxt "@title:group"
msgid "Compact Mode"
msgstr "Руҷаи ҷафс"

#: package/contents/ui/config/ConfigAppearance.qml:43
#, fuzzy, kde-format
#| msgctxt "@label:listbox"
#| msgid "Temperature:"
msgctxt "@label"
msgid "Show temperature:"
msgstr "Ҳарорат:"

#: package/contents/ui/config/ConfigAppearance.qml:46
#, kde-format
msgctxt "@option:radio Show temperature:"
msgid "Over the widget icon"
msgstr ""

#: package/contents/ui/config/ConfigAppearance.qml:54
#, kde-format
msgctxt "@option:radio Show temperature:"
msgid "Beside the widget icon"
msgstr ""

#: package/contents/ui/config/ConfigAppearance.qml:61
#, kde-format
msgctxt "@option:radio Show temperature:"
msgid "Do not show"
msgstr ""

#: package/contents/ui/config/ConfigAppearance.qml:70
#, kde-format
msgctxt "@label"
msgid "Show in tooltip:"
msgstr ""

#: package/contents/ui/config/ConfigAppearance.qml:71
#, kde-format
msgctxt "@option:check"
msgid "Temperature"
msgstr "Ҳарорат"

#: package/contents/ui/config/ConfigAppearance.qml:76
#, kde-format
msgctxt "@option:check Show in tooltip: wind"
msgid "Wind"
msgstr "Шамол"

#: package/contents/ui/config/ConfigAppearance.qml:81
#, kde-format
msgctxt "@option:check Show in tooltip: pressure"
msgid "Pressure"
msgstr "Фишор"

#: package/contents/ui/config/ConfigAppearance.qml:86
#, kde-format
msgctxt "@option:check Show in tooltip: humidity"
msgid "Humidity"
msgstr "Намӣ"

#: package/contents/ui/config/ConfigUnits.qml:32
#, kde-format
msgctxt "@label:listbox"
msgid "Temperature:"
msgstr "Ҳарорат:"

#: package/contents/ui/config/ConfigUnits.qml:38
#, kde-format
msgctxt "@label:listbox"
msgid "Pressure:"
msgstr "Фишор:"

#: package/contents/ui/config/ConfigUnits.qml:44
#, kde-format
msgctxt "@label:listbox"
msgid "Wind speed:"
msgstr "Суръати шамол:"

#: package/contents/ui/config/ConfigUnits.qml:50
#, kde-format
msgctxt "@label:listbox"
msgid "Visibility:"
msgstr "Намоёнӣ:"

#: package/contents/ui/config/ConfigWeatherStation.qml:50
#, kde-format
msgctxt "@label:spinbox"
msgid "Update every:"
msgstr "Навсози кардан ҳар як:"

#: package/contents/ui/config/ConfigWeatherStation.qml:53
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 дақиқа"
msgstr[1] "%1 дақиқа"

#: package/contents/ui/config/ConfigWeatherStation.qml:65
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "Ҷойгиршавӣ:"

#: package/contents/ui/config/ConfigWeatherStation.qml:71
#, kde-format
msgctxt "No location is currently selected"
msgid "None selected"
msgstr ""

#: package/contents/ui/config/ConfigWeatherStation.qml:75
#, kde-format
msgctxt "@label"
msgid "Provider:"
msgstr ""

#: package/contents/ui/config/ConfigWeatherStation.qml:99
#, fuzzy, kde-format
#| msgctxt "@info:placeholder"
#| msgid "Enter location"
msgctxt "@info:placeholder"
msgid "Enter new location"
msgstr "Ҷойгиршавиро ворид намоед"

#: package/contents/ui/config/ConfigWeatherStation.qml:99
#, kde-format
msgctxt "@info:placeholder"
msgid "Enter location"
msgstr "Ҷойгиршавиро ворид намоед"

#: package/contents/ui/config/ConfigWeatherStation.qml:183
#, kde-format
msgctxt "@info"
msgid "No weather stations found for '%1'"
msgstr "Ягон дастгоҳи обу ҳаво барои '%1' ёфт нашуд"

#: package/contents/ui/config/ConfigWeatherStation.qml:185
#, kde-format
msgctxt "@info"
msgid "Search for a weather station to change your location"
msgstr ""

#: package/contents/ui/config/ConfigWeatherStation.qml:187
#, kde-format
msgctxt "@info"
msgid "Search for a weather station to set your location"
msgstr ""

#: package/contents/ui/ForecastView.qml:73
#: package/contents/ui/ForecastView.qml:80
#, kde-format
msgctxt "Short for no data available"
msgid "-"
msgstr "-"

#: package/contents/ui/FullRepresentation.qml:37
#, kde-format
msgid "Please set your location"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:40
#, fuzzy, kde-format
#| msgctxt "@info:placeholder"
#| msgid "Enter location"
msgid "Set location…"
msgstr "Ҷойгиршавиро ворид намоед"

#: package/contents/ui/FullRepresentation.qml:55
#, kde-format
msgid "Weather information retrieval for %1 timed out."
msgstr ""

#: package/contents/ui/main.qml:95
#, kde-format
msgctxt "pressure tendency"
msgid "Rising"
msgstr "Болоравӣ"

#: package/contents/ui/main.qml:96
#, kde-format
msgctxt "pressure tendency"
msgid "Falling"
msgstr "Поёнравӣ"

#: package/contents/ui/main.qml:97
#, kde-format
msgctxt "pressure tendency"
msgid "Steady"
msgstr "Устувор"

#: package/contents/ui/main.qml:119
#, kde-format
msgctxt "Wind condition"
msgid "Calm"
msgstr "Ором"

#: package/contents/ui/main.qml:157
#, kde-format
msgctxt "Forecast period timeframe"
msgid "1 Day"
msgid_plural "%1 Days"
msgstr[0] "1 рӯз"
msgstr[1] "%1 рӯз"

#: package/contents/ui/main.qml:179
#, kde-format
msgctxt "@label"
msgid "Windchill:"
msgstr "Хунукии шамол:"

#: package/contents/ui/main.qml:186
#, kde-format
msgctxt "@label"
msgid "Humidex:"
msgstr ""

#: package/contents/ui/main.qml:193
#, kde-format
msgctxt "@label ground temperature"
msgid "Dewpoint:"
msgstr "Нуқтаи шабнам:"

#: package/contents/ui/main.qml:200
#, kde-format
msgctxt "@label"
msgid "Pressure:"
msgstr "Фишор:"

#: package/contents/ui/main.qml:207
#, kde-format
msgctxt "@label pressure tendency, rising/falling/steady"
msgid "Pressure Tendency:"
msgstr "Тамоюли фишор:"

#: package/contents/ui/main.qml:214
#, kde-format
msgctxt "@label"
msgid "Visibility:"
msgstr "Намоёнӣ:"

#: package/contents/ui/main.qml:221
#, kde-format
msgctxt "@label"
msgid "Humidity:"
msgstr "Намӣ:"

#: package/contents/ui/main.qml:228
#, kde-format
msgctxt "@label"
msgid "Wind Gust:"
msgstr "Шадиди шамол:"

#: package/contents/ui/main.qml:248
#, fuzzy, kde-format
#| msgctxt "Forecast period timeframe"
#| msgid "1 Day"
#| msgid_plural "%1 Days"
msgctxt "Time of the day (from the duple Day/Night)"
msgid "Day"
msgstr "1 рӯз"

#: package/contents/ui/main.qml:249
#, kde-format
msgctxt "Time of the day (from the duple Day/Night)"
msgid "Night"
msgstr ""

#: package/contents/ui/main.qml:287
#, kde-format
msgctxt "certain weather condition (probability percentage)"
msgid "%1 (%2 %)"
msgstr "%1 (%2 %)"

#: package/contents/ui/main.qml:388
#, kde-format
msgctxt "@info:tooltip %1 is the translated plasmoid name"
msgid "Click to configure %1"
msgstr ""

#: package/contents/ui/main.qml:399
#, kde-format
msgctxt "weather condition + temperature"
msgid "%1 %2"
msgstr "%1 %2"

#: package/contents/ui/main.qml:406
#, kde-format
msgctxt "winddirection windspeed (windgust)"
msgid "%1 %2 (%3)"
msgstr "%1 %2 (%3)"

#: package/contents/ui/main.qml:409
#, kde-format
msgctxt "winddirection windspeed"
msgid "%1 %2"
msgstr "%1 %2"

#: package/contents/ui/main.qml:418
#, kde-format
msgctxt "pressure (tendency)"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: package/contents/ui/main.qml:425
#, kde-format
msgid "Humidity: %1"
msgstr "Намӣ: %1"

#: package/contents/ui/NoticesView.qml:60
#, kde-format
msgctxt "@action:button"
msgid "Show more information"
msgstr ""

#: package/contents/ui/SwitchPanel.qml:32
#, kde-format
msgctxt "@title:tab"
msgid "Details"
msgstr "Тафсилот"

#: package/contents/ui/SwitchPanel.qml:39
#, fuzzy, kde-format
#| msgctxt "@title:tab"
#| msgid "Notices"
msgctxt ""
"@title:tab %1 is the number of weather notices (alerts, warnings, "
"watches, ...) issued"
msgid "%1 Notice"
msgid_plural "%1 Notices"
msgstr[0] "Тавзеҳ"
msgstr[1] "Тавзеҳ"

#: plugin/locationlistmodel.cpp:41 plugin/locationlistmodel.cpp:69
#, kde-format
msgid "Cannot find '%1' using %2."
msgstr "'%1' ба воситаи %2 ёфт намешавад."

#: plugin/locationlistmodel.cpp:66
#, kde-format
msgid "Connection to %1 weather server timed out."
msgstr ""
"Вақти мунтазири пайвастшавӣ ба хизматрасони обу ҳавои %1 ба анҷом расид."

#: plugin/locationlistmodel.cpp:125
#, kde-format
msgctxt "A weather station location and the weather service it comes from"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: plugin/util.cpp:44
#, kde-format
msgctxt "Degree, unit symbol"
msgid "°"
msgstr "°"

#: plugin/util.cpp:48 plugin/util.cpp:52
#, kde-format
msgctxt "temperature unitsymbol"
msgid "%1 %2"
msgstr "%1 %2"

#: plugin/util.cpp:62
#, kde-format
msgctxt "value unitsymbol"
msgid "%1 %2"
msgstr "%1 %2"

#: plugin/util.cpp:68
#, kde-format
msgctxt "value percentsymbol"
msgid "%1 %"
msgstr "%1 %"

#: plugin/util.cpp:74
#, fuzzy, kde-format
#| msgctxt "A weather station location and the weather service it comes from"
#| msgid "%1 (%2)"
msgctxt "@item %1 is a unit description and %2 its unit symbol"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#, fuzzy
#~| msgctxt "@action:button"
#~| msgid "Choose..."
#~ msgctxt "@action:button"
#~ msgid "Choose…"
#~ msgstr "Интихоб намоед..."

#~ msgctxt "@title:window"
#~ msgid "Select Weather Station"
#~ msgstr "Дастгоҳи обу ҳаворо интихоб намоед"

#~ msgctxt "@action:button"
#~ msgid "Select"
#~ msgstr "Интихоб кардан"

#~ msgctxt "@action:button"
#~ msgid "Cancel"
#~ msgstr "Бекор кардан"

#~ msgctxt "@option:check Show on widget icon: temperature"
#~ msgid "Temperature"
#~ msgstr "Ҳарорат"

#~ msgctxt "@info:tooltip"
#~ msgid "Please configure"
#~ msgstr "Лутфан, танзим кунед"

#~ msgctxt "weather services provider name (id)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#, fuzzy
#~| msgctxt "@info:tooltip"
#~| msgid "Select weather services providers"
#~ msgid "Weather providers:"
#~ msgstr "Интихоб кардани таъмингари хадамоти обу ҳаво"

#~ msgctxt "@action:button"
#~ msgid "Search"
#~ msgstr "Ҷустуҷӯ"

#~ msgctxt "@action:button"
#~ msgid "Configure..."
#~ msgstr "Танзимот..."

#~ msgctxt "@item"
#~ msgid "Celsius °C"
#~ msgstr "Селсий °C"

#~ msgctxt "@item"
#~ msgid "Fahrenheit °F"
#~ msgstr "Фаренгейт °F"

#~ msgctxt "@item"
#~ msgid "Meters per Second m/s"
#~ msgstr "Метр дар як сония м/с"

#~ msgctxt "@item"
#~ msgid "Miles per Hour mph"
#~ msgstr "Мил дар як соат mph"

#~ msgctxt "@item"
#~ msgid "Kilometers"
#~ msgstr "Километр"

#~ msgctxt "@item"
#~ msgid "Miles"
#~ msgstr "Мил"
