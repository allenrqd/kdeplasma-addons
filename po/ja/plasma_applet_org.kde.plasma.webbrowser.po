msgid ""
msgstr ""
"Project-Id-Version: kdeplasma-addons\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-17 01:38+0000\n"
"PO-Revision-Date: 2022-10-21 20:50-0700\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: package/contents/config/config.qml:12
#, kde-format
msgctxt "@title"
msgid "General"
msgstr ""

#: package/contents/config/config.qml:18
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:31
#, kde-format
msgctxt "@title:group"
msgid "Icon:"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:32
#, kde-format
msgctxt "@option:radio"
msgid "Website's favicon"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:55
#, kde-format
msgctxt "@action:button"
msgid "Change Web Browser's icon"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:56
#, kde-format
msgctxt "@info:whatsthis"
msgid ""
"Current icon is %1. Click to open menu to change the current icon or reset "
"to the default icon."
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:60
#, kde-format
msgctxt "@info:tooltip"
msgid "Icon name is \"%1\""
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:95
#, kde-format
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose…"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:97
#, kde-format
msgctxt "@info:whatsthis"
msgid "Choose an icon for Web Browser"
msgstr ""

#: package/contents/ui/ConfigAppearance.qml:101
#, kde-format
msgctxt "@item:inmenu Reset icon to default"
msgid "Reset to default icon"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:18
#, kde-format
msgctxt "@title:group"
msgid "Content scaling:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:22
#, kde-format
msgctxt "@option:radio"
msgid "Fixed scale:"
msgstr ""

#: package/contents/ui/ConfigGeneral.qml:56
#, kde-format
msgctxt "@option:radio"
msgid "Automatic scaling if width is below"
msgstr ""

#: package/contents/ui/main.qml:66
#, kde-format
msgctxt "@action:button"
msgid "Go Back"
msgstr ""

#: package/contents/ui/main.qml:73
#, kde-format
msgctxt "@action:button"
msgid "Go Forward"
msgstr ""

#: package/contents/ui/main.qml:92
#, kde-format
msgctxt "@info"
msgid "Type a URL"
msgstr ""

#: package/contents/ui/main.qml:128
#, kde-format
msgctxt "@action:button"
msgid "Stop Loading This Page"
msgstr ""

#: package/contents/ui/main.qml:128
#, kde-format
msgctxt "@action:button"
msgid "Reload This Page"
msgstr ""

#: package/contents/ui/main.qml:178
#, kde-format
msgctxt "@action:inmenu"
msgid "Open Link in Browser"
msgstr ""

#: package/contents/ui/main.qml:184
#, kde-format
msgctxt "@action:inmenu"
msgid "Copy Link Address"
msgstr ""

#: package/contents/ui/main.qml:246
#, kde-format
msgctxt "An unwanted popup was blocked"
msgid "Popup blocked"
msgstr ""

#: package/contents/ui/main.qml:247
#, kde-format
msgid ""
"Click here to open the following blocked popup:\n"
"%1"
msgstr ""
